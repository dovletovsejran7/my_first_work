import threading
from pywebio import start_server
from pywebio.input import *
from pywebio.output import *
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, Session, sessionmaker, Query
from pywebio.session import defer_call, info as session_info, run_async, run_js
import time


engine = create_engine("mysql+pymysql://root:22022006murkaosman@localhost/users")

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    login = Column(VARCHAR(100), nullable=False, unique=True)
    email = Column(VARCHAR(100), nullable=False, unique=True)
    password = Column(VARCHAR(200), nullable=False)

chat_msgs = []
user_name = None
stop = False
ms_box = []

MAX_MESSAGES_COUNT = 100


class Login_or_registr:
    def login(self):
            session = Session(bind=engine)

            account = User(login=input("Введите логин:"), email=input("Введите электронную почту: "),
                           password=input("Введите пароль:"))
            global user_name
            user_name = account.login
            session.add(account)
            session.commit()


    def sign_in(self):
        global user_name
        while user_name == None:
            try:
                session = Session(bind=engine)

                account = [input("Введите электронную почту: "), input("Введите пароль:")]

                self.resoult = session.query(User).filter(User.email == account[0]).one()
                if self.resoult.password == account[1]:
                    user_name = self.resoult.login
                session.commit()
            except:
                print("Нету такого аккаунта")


def main():
    l_g = Login_or_registr()
    login_reg = input("регистрация или вход")
    if login_reg == "регистрация":
        l_g.login()
    elif login_reg == "вход":
        l_g.sign_in()
    else:
        print("Неверно написанно регистрация или вход\nПопробуйте заново")

    global chat_msgs
    global user_name
    global  ms_box

    put_markdown("Добро пожаловать в онлайн чат!")

    msg_box = output()
    put_scrollable(msg_box, height=300, keep_bottom=True)
    chat_msgs.append(('🔊', f'`{user_name}` присоединился к чату!'))
    msg_box.append(put_markdown(f'🔊 `{user_name}` присоединился к чату'))
    try:
        refresh_task = threading.Thread(target=refresh_msg, args=(user_name, msg_box), daemon=True)
        refresh_task.start()
    except:
        pass


    while True:
        data = input_group("💭 Новое сообщение", [
            input(placeholder="Текст сообщения ...", name="msg"),
            actions(name="cmd", buttons=["Отправить", {'label': "Выйти из чата", 'type': 'cancel'}])
        ], validate=lambda m: ('msg', "Введите текст сообщения!") if m["cmd"] == "Отправить" and not m[
            'msg'] else None)

        if data is None:
            break

        msg_box.append(put_markdown(f"`{user_name}`: {data['msg']}"))
        chat_msgs.append((user_name, data['msg']))



    user_name = None
    global stop
    stop = True
    toast("Вы вышли из чата!")
    msg_box.append(put_markdown(f'🔊 Пользователь `{user_name}` покинул чат!'))
    chat_msgs.append(('🔊', f'Пользователь `{user_name}` покинул чат!'))

    put_buttons(['Перезайти'], onclick=lambda btn: run_js('window.location.reload()'))

def refresh_msg(nickname, msg_box):
    try:
        global chat_msgs
        global ms_box
        last_idx = len(chat_msgs)

        while stop == False:
            time.sleep(1)

            for m in chat_msgs[last_idx:]:
                if m[0] != nickname:
                    msg_box.append(put_markdown(f"`{m[0]}`: {m[1]}"))

            if len(chat_msgs) > MAX_MESSAGES_COUNT:
                chat_msgs = chat_msgs[len(chat_msgs) // 2:]

            last_idx = len(chat_msgs)
    except:
        pass


if __name__ == "__main__":
    start_server(main, debug=True, port=8080, cdn=False)
